﻿using CasaRural.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Linq;

namespace CasaRural.Filter
{
    public class AdminPrincipalFilter : IActionFilter
    {
        private readonly casaruralContext _context;

        public AdminPrincipalFilter(casaruralContext context)
        {
            _context = context;
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
           
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var user = _context.UserAccount.Where(u => u.ID == Convert.ToInt32(context.HttpContext.Session.GetString("id"))).FirstOrDefault();
            var role = _context.Role.Where(a => a.ID == user.RoleID).FirstOrDefault();
            

            if (!role.Name.Equals("Admin"))
            {
                if(!role.Name.Equals("Director"))
                context.Result = new RedirectToActionResult("Index", "Login", null);
            }
        }
    }
}
