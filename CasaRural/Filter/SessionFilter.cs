﻿using CasaRural.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Linq;

namespace CasaRural.Filter
{
    //Primero heredamos de IActionFilter
    public class SessionFilter : IActionFilter
    {
        private readonly casaruralContext _context;

        public SessionFilter(casaruralContext context)
        {
            _context = context;
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var user = _context.UserAccount.Where(u => u.ID == Convert.ToInt32(context.HttpContext.Session.GetString("id"))).FirstOrDefault();

            if(user == null)
            {
                context.Result = new RedirectToActionResult("Index", "Login", null);
            }
        }
    }
}
