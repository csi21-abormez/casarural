﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CasaRural.Migrations
{
    public partial class migration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Diet",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Price = table.Column<double>(nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Diet", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Furniture",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    BuyDate = table.Column<DateTime>(nullable: false),
                    High = table.Column<double>(nullable: false),
                    Weight = table.Column<double>(nullable: false),
                    Image = table.Column<string>(nullable: true),
                    Available = table.Column<bool>(nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Furniture", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "JobPosition",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(maxLength: 100, nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobPosition", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Menu",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Controller = table.Column<string>(nullable: false),
                    Action = table.Column<string>(nullable: false),
                    Label = table.Column<string>(nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Menu", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Other",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Price = table.Column<double>(nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Other", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Role",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Role", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "TypeRoom",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: false),
                    Price = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TypeRoom", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "RoleHasMenu",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    MenuID = table.Column<int>(nullable: false),
                    RoleID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleHasMenu", x => x.ID);
                    table.ForeignKey(
                        name: "FK_RoleHasMenu_Menu_MenuID",
                        column: x => x.MenuID,
                        principalTable: "Menu",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RoleHasMenu_Role_RoleID",
                        column: x => x.RoleID,
                        principalTable: "Role",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserAccount",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Username = table.Column<string>(maxLength: 10, nullable: false),
                    Password = table.Column<string>(nullable: false),
                    Email = table.Column<string>(maxLength: 50, nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    RoleID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserAccount", x => x.ID);
                    table.ForeignKey(
                        name: "FK_UserAccount_Role_RoleID",
                        column: x => x.RoleID,
                        principalTable: "Role",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Room",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    NumberRoom = table.Column<int>(nullable: false),
                    Image = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    TypeRoomID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Room", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Room_TypeRoom_TypeRoomID",
                        column: x => x.TypeRoomID,
                        principalTable: "TypeRoom",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Admin",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Surname = table.Column<string>(maxLength: 80, nullable: false),
                    Phone = table.Column<string>(nullable: false),
                    UserAccountID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Admin", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Admin_UserAccount_UserAccountID",
                        column: x => x.UserAccountID,
                        principalTable: "UserAccount",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Customer",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Surname = table.Column<string>(maxLength: 80, nullable: false),
                    Phone = table.Column<string>(nullable: false),
                    UserAccountID = table.Column<int>(nullable: false),
                    Birthday = table.Column<DateTime>(nullable: false),
                    DNI = table.Column<string>(nullable: false),
                    Address = table.Column<string>(maxLength: 100, nullable: false),
                    City = table.Column<string>(maxLength: 100, nullable: false),
                    PostalCode = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customer", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Customer_UserAccount_UserAccountID",
                        column: x => x.UserAccountID,
                        principalTable: "UserAccount",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Employed",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Surname = table.Column<string>(maxLength: 80, nullable: false),
                    Phone = table.Column<string>(nullable: false),
                    UserAccountID = table.Column<int>(nullable: false),
                    Age = table.Column<int>(nullable: false),
                    Salary = table.Column<double>(nullable: false),
                    JobPostionID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employed", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Employed_JobPosition_JobPostionID",
                        column: x => x.JobPostionID,
                        principalTable: "JobPosition",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Employed_UserAccount_UserAccountID",
                        column: x => x.UserAccountID,
                        principalTable: "UserAccount",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Inventory",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    FurnitureID = table.Column<int>(nullable: false),
                    RoomID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Inventory", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Inventory_Furniture_FurnitureID",
                        column: x => x.FurnitureID,
                        principalTable: "Furniture",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Inventory_Room_RoomID",
                        column: x => x.RoomID,
                        principalTable: "Room",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Booking",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    BookingStartDate = table.Column<DateTime>(nullable: false),
                    BookingEndDate = table.Column<DateTime>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    CustomerID = table.Column<int>(nullable: false),
                    RoomID = table.Column<int>(nullable: false),
                    DietID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Booking", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Booking_Customer_CustomerID",
                        column: x => x.CustomerID,
                        principalTable: "Customer",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Booking_Diet_DietID",
                        column: x => x.DietID,
                        principalTable: "Diet",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Booking_Room_RoomID",
                        column: x => x.RoomID,
                        principalTable: "Room",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RoomCleaning",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CleanDate = table.Column<DateTime>(nullable: false),
                    Observations = table.Column<string>(maxLength: 50, nullable: true),
                    Clean = table.Column<bool>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    EmployedID = table.Column<int>(nullable: false),
                    RoomID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoomCleaning", x => x.ID);
                    table.ForeignKey(
                        name: "FK_RoomCleaning_Employed_EmployedID",
                        column: x => x.EmployedID,
                        principalTable: "Employed",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RoomCleaning_Room_RoomID",
                        column: x => x.RoomID,
                        principalTable: "Room",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Supplement",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    OtherID = table.Column<int>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    BookingID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Supplement", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Supplement_Booking_BookingID",
                        column: x => x.BookingID,
                        principalTable: "Booking",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Supplement_Other_OtherID",
                        column: x => x.OtherID,
                        principalTable: "Other",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Diet",
                columns: new[] { "ID", "Active", "Name", "Price" },
                values: new object[,]
                {
                    { 1, true, "Full board", 50.0 },
                    { 2, true, "Hall board", 30.0 },
                    { 3, true, "only breakfast", 15.0 },
                    { 4, true, "accommodation only", 0.0 }
                });

            migrationBuilder.InsertData(
                table: "JobPosition",
                columns: new[] { "ID", "Active", "Description", "Name" },
                values: new object[,]
                {
                    { 8, true, "Encargado de la correcta limpieza de las habitaciones", "Limpiador" },
                    { 7, true, "Encargado del correcto orden y limpieza de la casa rural", "Gobernante" },
                    { 5, true, "Encargado del mantenimiento de la casa rural", "Jefe de Mantenimiento" },
                    { 6, true, "Encargado de la recepción de la casa rural", "Recepcionista" },
                    { 3, true, "Encargado de la elaboración de las dietas", "Jefe Cocina" },
                    { 2, true, "Encargado de la subdirección de la empresa", "Subdirector" },
                    { 1, true, "Encargado de la dirección de la empresa", "Director" },
                    { 4, true, "Encargado de la administración de la empresa", "Administración" }
                });

            migrationBuilder.InsertData(
                table: "Menu",
                columns: new[] { "ID", "Action", "Active", "Controller", "Label" },
                values: new object[,]
                {
                    { 10, "Index", true, "RoleHasMenus", "RoleHasMenu" },
                    { 17, "Index", true, "Other", "Other" },
                    { 16, "Index", true, "UserAccounts", "UserAccount" },
                    { 15, "Index", true, "TypeRooms", "TypeRoom" },
                    { 14, "Index", true, "Supplements", "Supplement" },
                    { 13, "Index", true, "Rooms", "Room" },
                    { 12, "Index", true, "RoomCleanings", "RoomCleaning" },
                    { 9, "Index", true, "Menus", "Menu" },
                    { 11, "Index", true, "Roles", "Role" },
                    { 7, "Index", true, "Inventories", "Inventory" },
                    { 8, "Index", true, "JobPositions", "JobPosition" },
                    { 6, "Index", true, "Furnitures", "Furniture" },
                    { 5, "Index", true, "Employeds", "Employed" },
                    { 4, "Index", true, "Diets", "Diet" },
                    { 3, "Index", true, "Customers", "Customer" },
                    { 2, "Index", true, "Bookings", "Booking" },
                    { 1, "Index", true, "Admins", "Admin" }
                });

            migrationBuilder.InsertData(
                table: "Other",
                columns: new[] { "ID", "Active", "Name", "Price" },
                values: new object[,]
                {
                    { 4, true, "Consumición alcohol premium minibar", 5.5 },
                    { 3, true, "Consumición alcohol minibar", 3.5 },
                    { 1, true, "Consumición agua minibar", 1.5 },
                    { 2, true, "Consumición refresco minibar", 2.5 }
                });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "ID", "Active", "Description", "Name" },
                values: new object[,]
                {
                    { 7, true, "Encargado de la elaboración de las dietas", "Jefe de Mantenimiento" },
                    { 10, true, "Encargado de la correcta limpieza de las habitaciones", "Limpiador" },
                    { 9, true, "Encargado del correcto orden y limpieza de la casa rural", "Gobernante" },
                    { 8, true, "Encargado de la recepción de la casa rural", "Recepcionista" },
                    { 6, true, "Encargado de la administración de la empresa", "Administracion" },
                    { 1, true, "Rol de Admin", "Admin" },
                    { 4, true, "Rol de Encargado de la subdirección de la empresa", "Subdirector" },
                    { 3, true, "Rol de Employed", "Director" },
                    { 2, true, "Rol de Customer", "Customer" },
                    { 5, true, "Encargado de la elaboración de las dietas", "Jefe Cocina" }
                });

            migrationBuilder.InsertData(
                table: "TypeRoom",
                columns: new[] { "ID", "Active", "Description", "Name", "Price" },
                values: new object[,]
                {
                    { 2, true, "Habitación de matrimonio", "Double", 60.0 },
                    { 1, true, "Suite", "Suite", 95.0 },
                    { 3, true, "Habitación simple para una persona", "Singgle", 45.0 }
                });

            migrationBuilder.InsertData(
                table: "RoleHasMenu",
                columns: new[] { "ID", "Active", "MenuID", "RoleID" },
                values: new object[,]
                {
                    { 1, true, 1, 1 },
                    { 17, true, 17, 1 },
                    { 16, true, 16, 1 },
                    { 15, true, 15, 1 },
                    { 14, true, 14, 1 },
                    { 13, true, 13, 1 },
                    { 12, true, 12, 1 },
                    { 10, true, 10, 1 },
                    { 11, true, 11, 1 },
                    { 8, true, 8, 1 },
                    { 7, true, 7, 1 },
                    { 6, true, 6, 1 },
                    { 5, true, 5, 1 },
                    { 4, true, 4, 1 },
                    { 3, true, 3, 1 },
                    { 2, true, 2, 1 },
                    { 9, true, 9, 1 }
                });

            migrationBuilder.InsertData(
                table: "UserAccount",
                columns: new[] { "ID", "Active", "Email", "Password", "RoleID", "Username" },
                values: new object[,]
                {
                    { 2, true, "customer@email.es", "MQAyADMANAA=", 2, "customer" },
                    { 1, true, "admin@email.es", "QQBsAHQAYQBpAHIAMQAyADMAJAAlAA==", 1, "admin" },
                    { 4, false, "admin@email.es", "QQBsAHQAYQBpAHIAMQAyADMAJAAlAA==", 1, "admin2" },
                    { 3, true, "Director@email.es", "MQAyADMANAA=", 3, "Director" }
                });

            migrationBuilder.InsertData(
                table: "Admin",
                columns: new[] { "ID", "Name", "Phone", "Surname", "UserAccountID" },
                values: new object[,]
                {
                    { 1, "admin", "655555555", "admin", 1 },
                    { 2, "admin2", "655555555", "admin2", 4 }
                });

            migrationBuilder.InsertData(
                table: "Customer",
                columns: new[] { "ID", "Address", "Birthday", "City", "DNI", "Name", "Phone", "PostalCode", "Surname", "UserAccountID" },
                values: new object[] { 1, "C/prueba", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sevilla", "00000000L", "customer", "655555555", "41111", "customer", 3 });

            migrationBuilder.InsertData(
                table: "Employed",
                columns: new[] { "ID", "Age", "JobPostionID", "Name", "Phone", "Salary", "Surname", "UserAccountID" },
                values: new object[] { 1, 30, 1, "Director", "655555555", 50000.0, "employed", 2 });

            migrationBuilder.CreateIndex(
                name: "IX_Admin_UserAccountID",
                table: "Admin",
                column: "UserAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Booking_CustomerID",
                table: "Booking",
                column: "CustomerID");

            migrationBuilder.CreateIndex(
                name: "IX_Booking_DietID",
                table: "Booking",
                column: "DietID");

            migrationBuilder.CreateIndex(
                name: "IX_Booking_RoomID",
                table: "Booking",
                column: "RoomID");

            migrationBuilder.CreateIndex(
                name: "IX_Customer_UserAccountID",
                table: "Customer",
                column: "UserAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Employed_JobPostionID",
                table: "Employed",
                column: "JobPostionID");

            migrationBuilder.CreateIndex(
                name: "IX_Employed_UserAccountID",
                table: "Employed",
                column: "UserAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Inventory_FurnitureID",
                table: "Inventory",
                column: "FurnitureID");

            migrationBuilder.CreateIndex(
                name: "IX_Inventory_RoomID",
                table: "Inventory",
                column: "RoomID");

            migrationBuilder.CreateIndex(
                name: "IX_RoleHasMenu_MenuID",
                table: "RoleHasMenu",
                column: "MenuID");

            migrationBuilder.CreateIndex(
                name: "IX_RoleHasMenu_RoleID",
                table: "RoleHasMenu",
                column: "RoleID");

            migrationBuilder.CreateIndex(
                name: "IX_Room_TypeRoomID",
                table: "Room",
                column: "TypeRoomID");

            migrationBuilder.CreateIndex(
                name: "IX_RoomCleaning_EmployedID",
                table: "RoomCleaning",
                column: "EmployedID");

            migrationBuilder.CreateIndex(
                name: "IX_RoomCleaning_RoomID",
                table: "RoomCleaning",
                column: "RoomID");

            migrationBuilder.CreateIndex(
                name: "IX_Supplement_BookingID",
                table: "Supplement",
                column: "BookingID");

            migrationBuilder.CreateIndex(
                name: "IX_Supplement_OtherID",
                table: "Supplement",
                column: "OtherID");

            migrationBuilder.CreateIndex(
                name: "IX_UserAccount_RoleID",
                table: "UserAccount",
                column: "RoleID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Admin");

            migrationBuilder.DropTable(
                name: "Inventory");

            migrationBuilder.DropTable(
                name: "RoleHasMenu");

            migrationBuilder.DropTable(
                name: "RoomCleaning");

            migrationBuilder.DropTable(
                name: "Supplement");

            migrationBuilder.DropTable(
                name: "Furniture");

            migrationBuilder.DropTable(
                name: "Menu");

            migrationBuilder.DropTable(
                name: "Employed");

            migrationBuilder.DropTable(
                name: "Booking");

            migrationBuilder.DropTable(
                name: "Other");

            migrationBuilder.DropTable(
                name: "JobPosition");

            migrationBuilder.DropTable(
                name: "Customer");

            migrationBuilder.DropTable(
                name: "Diet");

            migrationBuilder.DropTable(
                name: "Room");

            migrationBuilder.DropTable(
                name: "UserAccount");

            migrationBuilder.DropTable(
                name: "TypeRoom");

            migrationBuilder.DropTable(
                name: "Role");
        }
    }
}
