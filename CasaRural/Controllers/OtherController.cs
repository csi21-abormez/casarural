﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CasaRural.Data;
using CasaRural.Models;
using CasaRural.Filter;

namespace CasaRural.Controllers
{
    [ServiceFilter(typeof(SessionFilter))]
    public class OtherController : Controller
    {
        private readonly casaruralContext _context;

        public OtherController(casaruralContext context)
        {
            _context = context;
        }

        // GET: Other
        public async Task<IActionResult> Index()
        {
            return View(await _context.Other.Where(o => o.Active == true).ToListAsync());
        }

        // GET: Other/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var other = await _context.Other
                .FirstOrDefaultAsync(m => m.ID == id);
            if (other == null)
            {
                return NotFound();
            }

            return View(other);
        }

        // GET: Other/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Other/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,Price,Active")] Other other)
        {
            if (ModelState.IsValid)
            {
                other.Active = true;
                _context.Add(other);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(other);
        }

        // GET: Other/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var other = await _context.Other.FindAsync(id);
            if (other == null)
            {
                return NotFound();
            }
            return View(other);
        }

        // POST: Other/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,Price,Active")] Other other)
        {
            if (id != other.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(other);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OtherExists(other.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(other);
        }

        // GET: Other/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var other = await _context.Other
                .FirstOrDefaultAsync(m => m.ID == id);
            if (other == null)
            {
                return NotFound();
            }

            return View(other);
        }

        // POST: Other/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var other = await _context.Other.FindAsync(id);
            _context.Other.Remove(other);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OtherExists(int id)
        {
            return _context.Other.Any(e => e.ID == id);
        }

        //Ver todo los registros existentes
        public async Task<IActionResult> VerTodoLosRegistros()
        {
            return View(await _context.Other.ToListAsync());
        }

        //Borrado lógico.
        public async Task<IActionResult> BorradoLogico(int? id)
        {

            var other = await _context.Other.FindAsync(id);

            other.Active = false;

            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }
    }
}
