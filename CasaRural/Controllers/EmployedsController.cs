﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CasaRural.Data;
using CasaRural.Models;
using CasaRural.Filter;


namespace CasaRural.Controllers
{
    public class EmployedsController : Controller
    {
        private readonly casaruralContext _context;

        public EmployedsController(casaruralContext context)
        {
            _context = context;
        }

        // GET: Employeds
        public async Task<IActionResult> Index(String busqueda)
        {
            var casaruralContext = _context.Employed.Include(e => e.JobPosition).Include(e => e.UserAccount).Where(u => u.UserAccount.Active == true);
            if (!String.IsNullOrEmpty(busqueda))
            {
                casaruralContext = _context.Employed.Include(c => c.UserAccount).Include(e => e.JobPosition).Where(f => f.Name.Contains(busqueda));
            }
            return View(await casaruralContext.ToListAsync());
        }

        // GET: Employeds/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employed = await _context.Employed
                .Include(e => e.JobPosition)
                .Include(e => e.UserAccount)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (employed == null)
            {
                return NotFound();
            }

            return View(employed);
        }

        // GET: Employeds/Create
        public IActionResult Create()
        {
            ViewData["JobPostionID"] = new SelectList(_context.JobPosition, "ID", "Name");
            ViewData["UserAccountID"] = new SelectList(_context.UserAccount, "ID", "Username");
            return View();
        }

        // POST: Employeds/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Age,Salary,JobPostionID,ID,Name,Surname,Phone,UserAccountID")] Employed employed)
        {
            if (ModelState.IsValid)
            {
                _context.Add(employed);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["JobPostionID"] = new SelectList(_context.JobPosition, "ID", "Name", employed.JobPostionID);
            ViewData["UserAccountID"] = new SelectList(_context.UserAccount, "ID", "Username", employed.UserAccountID);
            return View(employed);
        }

        // GET: Employeds/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employed = await _context.Employed.FindAsync(id);
            if (employed == null)
            {
                return NotFound();
            }
            ViewData["JobPostionID"] = new SelectList(_context.JobPosition, "ID", "Name", employed.JobPostionID);
            ViewData["UserAccountID"] = new SelectList(_context.UserAccount, "ID", "Username", employed.UserAccountID);
            return View(employed);
        }

        // POST: Employeds/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Age,Salary,JobPostionID,ID,Name,Surname,Phone,UserAccountID")] Employed employed)
        {
            if (id != employed.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(employed);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EmployedExists(employed.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["JobPostionID"] = new SelectList(_context.JobPosition, "ID", "Name", employed.JobPostionID);
            ViewData["UserAccountID"] = new SelectList(_context.UserAccount, "ID", "Username", employed.UserAccountID);
            return View(employed);
        }

        // GET: Employeds/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employed = await _context.Employed
                .Include(e => e.JobPosition)
                .Include(e => e.UserAccount)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (employed == null)
            {
                return NotFound();
            }

            return View(employed);
        }

        // POST: Employeds/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var employed = await _context.Employed.FindAsync(id);
            _context.Employed.Remove(employed);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EmployedExists(int id)
        {
            return _context.Employed.Any(e => e.ID == id);
        }

        //Ver todo los registros existentes
        public async Task<IActionResult> VerTodoLosRegistros()
        {
            var casaruralContext = _context.Employed.Include(e => e.JobPosition).Include(e => e.UserAccount);
            return View(await casaruralContext.ToListAsync());
        }

        //Borrado lógico.
        public async Task<IActionResult> BorradoLogico(int? id)
        {

            var employed = await _context.Employed.FindAsync(id);

            var useraccount = _context.UserAccount.Where(u => u.ID.Equals(employed.UserAccountID)).FirstOrDefault();

            useraccount.Active = false;

            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }
    }
}
