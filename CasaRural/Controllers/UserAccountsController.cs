﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CasaRural.Data;
using CasaRural.Models;
using CasaRural.Filter;

namespace CasaRural.Controllers
{
    
    [ServiceFilter(typeof(SessionFilter))]
    [ServiceFilter(typeof(AdminPrincipalFilter))]
    [ServiceFilter(typeof(AdminFilter))]
  
    public class UserAccountsController : Controller
    {
        private readonly casaruralContext _context;

        public UserAccountsController(casaruralContext context)
        {
            _context = context;
        }

        // GET: UserAccounts
        public async Task<IActionResult> Index(String busqueda)
        {
            var casaruralContext = _context.UserAccount.Include(u => u.Role).Where(u => u.Active == true);
            if (!String.IsNullOrEmpty(busqueda))
            {
                casaruralContext = _context.UserAccount.Where(f => f.Username.Contains(busqueda));
            }
            return View(await casaruralContext.ToListAsync());
        }

        // GET: UserAccounts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var userAccount = await _context.UserAccount
                .Include(u => u.Role)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (userAccount == null)
            {
                return NotFound();
            }

            return View(userAccount);
        }

        // GET: UserAccounts/Create
        public IActionResult Create()
        {
            ViewData["RoleID"] = new SelectList(_context.Role, "ID", "Name");
            return View();
        }

        // POST: UserAccounts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Username,Password,Email,Active,RoleID")] UserAccount userAccount)
        {
            if (ModelState.IsValid)
            {
                userAccount.Active = true;
                _context.Add(userAccount);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["RoleID"] = new SelectList(_context.Role, "ID", "Name", userAccount.RoleID);
            return View(userAccount);
        }

        // GET: UserAccounts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var userAccount = await _context.UserAccount.FindAsync(id);
            if (userAccount == null)
            {
                return NotFound();
            }
            ViewData["RoleID"] = new SelectList(_context.Role, "ID", "Name", userAccount.RoleID);
            return View(userAccount);
        }

        // POST: UserAccounts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Username,Password,Email,Active,RoleID")] UserAccount userAccount)
        {
            if (id != userAccount.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    userAccount.Password = Util.Encriptar(userAccount.Password);
                    _context.Update(userAccount);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UserAccountExists(userAccount.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["RoleID"] = new SelectList(_context.Role, "ID", "Name", userAccount.RoleID);
            return View(userAccount);
        }

        // GET: UserAccounts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var userAccount = await _context.UserAccount
                .Include(u => u.Role)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (userAccount == null)
            {
                return NotFound();
            }

            return View(userAccount);
        }

        // POST: UserAccounts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var userAccount = await _context.UserAccount.FindAsync(id);
            _context.UserAccount.Remove(userAccount);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UserAccountExists(int id)
        {
            return _context.UserAccount.Any(e => e.ID == id);
        }
        //Activa todas las cuentas de empleados.
        public async Task<IActionResult> ActivaTodo()
        {
            var rol = _context.Role.Where(r => r.Name == "Customer").FirstOrDefault();
            var ListuserAccount = _context.UserAccount.Where(u => u.Active == false && u.RoleID == rol.ID).ToList();

            foreach (UserAccount u in ListuserAccount)
            {
                u.Active = true;
                await _context.SaveChangesAsync();
            }


            return RedirectToAction(nameof(Index));
        }
        //Desactiva todas las cuentas de empleados.
        public async Task<IActionResult> DesactivaTodo()
        {
            var rol = _context.Role.Where(r => r.Name == "Customer").FirstOrDefault();
            var ListuserAccount = _context.UserAccount.Where(u => u.Active == true && u.RoleID == rol.ID).ToList();

            foreach (UserAccount u in ListuserAccount)
            {
                u.Active = false;
                await _context.SaveChangesAsync();
            }


            return RedirectToAction(nameof(Index));
        }

        // GET: UserAccounts
        public async Task<IActionResult> VerTodoLosRegistros()
        {
            var casaruralContext = _context.UserAccount.Include(u => u.Role);
            return View(await casaruralContext.ToListAsync());
        }

        //Borrado lógico.
        public async Task<IActionResult> BorradoLogico(int? id)
        {

            var userAccount = await _context.UserAccount.FindAsync(id);

            userAccount.Active = false;

            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }


    }
}
