﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CasaRural.Data;
using CasaRural.Models;
using CasaRural.Filter;

namespace CasaRural.Controllers
{
    [ServiceFilter(typeof(SessionFilter))]
    public class RoomsController : Controller
    {
        private readonly casaruralContext _context;

        public RoomsController(casaruralContext context)
        {
            _context = context;
        }

        // GET: Rooms
        public async Task<IActionResult> Index(String busquedaInicial, String busquedaFinal)
        {
            
            var casaruralContext = _context.Room.Include(r => r.TypeRoom).Where(r => r.Active == true);
            try
            {
                this.PlaningOcupacion(busquedaInicial, busquedaFinal);
            }
            catch(FormatException e)
            {
                TempData["ErrorMessage"] = "El formato de fecha no es correcto.";
                return RedirectToAction(nameof(Index));
            }
            return View(await casaruralContext.ToListAsync());
        }

        // GET: Rooms/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var room = await _context.Room
                .Include(r => r.TypeRoom)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (room == null)
            {
                return NotFound();
            }

            return View(room);
        }

        // GET: Rooms/Create
        public IActionResult Create()
        {
            ViewData["TypeRoomID"] = new SelectList(_context.TypeRoom, "ID", "Description");
            return View();
        }

        // POST: Rooms/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,NumberRoom,Image,TypeRoomID,Active")] Room room)
        {
            if (ModelState.IsValid)
            {
                room.Active = true;
                _context.Add(room);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["TypeRoomID"] = new SelectList(_context.TypeRoom, "ID", "Description", room.TypeRoomID);
            return View(room);
        }

        // GET: Rooms/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var room = await _context.Room.FindAsync(id);
            if (room == null)
            {
                return NotFound();
            }
            ViewData["TypeRoomID"] = new SelectList(_context.TypeRoom, "ID", "Description", room.TypeRoomID);
            return View(room);
        }

        // POST: Rooms/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,NumberRoom,Image,TypeRoomID,Active")] Room room)
        {
            if (id != room.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(room);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RoomExists(room.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["TypeRoomID"] = new SelectList(_context.TypeRoom, "ID", "Description", room.TypeRoomID);
            return View(room);
        }

        // GET: Rooms/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var room = await _context.Room
                .Include(r => r.TypeRoom)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (room == null)
            {
                return NotFound();
            }

            return View(room);
        }

        // POST: Rooms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var room = await _context.Room.FindAsync(id);
            _context.Room.Remove(room);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RoomExists(int id)
        {
            return _context.Room.Any(e => e.ID == id);
        }

        //Ver todo los registros existentes
        public async Task<IActionResult> VerTodoLosRegistros()
        {
            var casaruralContext = _context.Room.Include(r => r.TypeRoom);
            return View(await casaruralContext.ToListAsync());
        }

        //Borrado lógico.
        public async Task<IActionResult> BorradoLogico(int? id)
        {

            var room = await _context.Room.FindAsync(id);

            room.Active = false;

            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> PlaningOcupacion(String busquedaInicial, String busquedaFinal)
        {
            var rooms = new List<Room>();
            
           
            if (busquedaInicial != null && busquedaFinal != null)
            {
                var booking = _context.Booking.Include(b => b.Customer).Include(b => b.Diet).Include(b => b.Room).Where(b => b.BookingStartDate >= DateTime.Parse(busquedaInicial) && b.BookingEndDate <= DateTime.Parse(busquedaFinal)).ToList();
                foreach (Booking book in booking)
                {
                    rooms = _context.Room.Include(r => r.TypeRoom).Where(r => r.ID.Equals(book.RoomID)).ToList();
                  
                }
                ViewBag.booking = booking;

            }
            ViewData["BusquedaInicial"] = busquedaInicial;
            ViewData["BusquedaFinal"] = busquedaFinal;
            
           

            return View(rooms);
        }
    }
}
