﻿
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using CasaRural.Data;
using CasaRural.Models;


namespace CasaRural.Controllers
{
   
    public class LoginController : Controller
    {
        private readonly casaruralContext _context;

        public LoginController(casaruralContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Login(string Username, string Password)
        {

            string passwordByte = Util.Encriptar(Password);

            var usuario =  _context.UserAccount.Where(u => u.Username == Username && u.Password == passwordByte).FirstOrDefault();
            if (usuario != null)
            {
                //conseguimos el id y el nombre del usario
                HttpContext.Session.SetString("id", usuario.ID.ToString());
                HttpContext.Session.SetString("name", usuario.Username.ToString());
                var rol = _context.Role.Where(r => r.ID.Equals(usuario.RoleID)).FirstOrDefault();
                HttpContext.Session.SetString("rol", rol.Name.ToString());
                HttpContext.Session.SetString("rol", usuario.Role.Name.ToString());
                
                //cargamos el menu correspondiente
                Util.Menus = _context.RoleHasMenu.Include(m => m.Menu).Where(m => m.RoleID == usuario.RoleID).Select(m => m.Menu).ToList();
                return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Index", "Login");
            
        }

        public IActionResult Logout()
        {
            
            HttpContext.Session.Remove("id");
            HttpContext.Session.Remove("name");
            HttpContext.Session.Clear();
            Util.Menus = new List<Menu>();
            return RedirectToAction("Index", "Login");
        }
    }
}
