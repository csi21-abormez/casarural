﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CasaRural.Data;
using CasaRural.Models;
using CasaRural.dtos;
using Microsoft.EntityFrameworkCore;
using System;
using Microsoft.AspNetCore.Http;
using CasaRural.Filter;

namespace CasaRural.Controllers
{
    [ServiceFilter(typeof(SessionFilter))]

    public class CustomerDtoesController : Controller
    {
        private readonly casaruralContext _context;

        public CustomerDtoesController(casaruralContext context)
        {
            _context = context;
        }

       

        // GET: CustomerDtoes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CustomerDtoes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,Surname,Phone,Birthday,DNI,Address,City,postalCode,Level,Username,Password,Email,Active")] CustomerDto customerDto)
        {
            if (ModelState.IsValid)
            {

                var roleID = _context.Role.Where(r => r.Name.Equals("Customer")).First();
                

                //creamos los userAccount;
                UserAccount userAccount = new UserAccount();
                userAccount.Username = customerDto.Username;
                userAccount.Password = Util.Encriptar(customerDto.Password);
                userAccount.Email = customerDto.Email;
                userAccount.Active = true;
                userAccount.RoleID = roleID.ID;
                var existeAdmin = _context.Admin.Include(u => u.UserAccount).Where(u => u.UserAccount.Username.Equals(customerDto.Username)).ToList();
                var existeCustomer = _context.Customer.Include(u => u.UserAccount).Where(u => u.UserAccount.Username.Equals(customerDto.Username)).ToList();
                var existeEmployed = _context.Employed.Include(U => U.UserAccount).Where(u => u.UserAccount.Username.Equals(customerDto.Username)).ToList();
                if (existeAdmin.Count != 0 || existeCustomer.Count != 0 || existeEmployed.Count != 0)
                {
                    TempData["ErrorMessage"] = "Ya existe hay una cuenta con este nombre y contraseña.";
                    return RedirectToAction(nameof(Create));
                }
                _context.Add(userAccount);
                await _context.SaveChangesAsync();

                //creamos Customer;

                Customer customer = new Customer();
                customer.Name = customerDto.Name;
                customer.Surname = customerDto.Surname;
                customer.Phone = customerDto.Phone;
                customer.Address = customerDto.Address;
                customer.Birthday = customerDto.Birthday;
                customer.City = customerDto.City;
                customer.DNI = customerDto.DNI;
                customer.UserAccountID = userAccount.ID;
                customer.PostalCode = customerDto.postalCode;

               


                _context.Customer.Add(customer);
                await _context.SaveChangesAsync();
                if (HttpContext.Session.GetString("name") != null)
                {
                    return RedirectToAction("Index", "Customers");
                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }

                /*
                Util.SendEmail("csi21-abormez@colegioaltair.net", customerDto.Name,"Prueba de email");

                return RedirectToAction("Index", "Login");
                */
            }
            return RedirectToAction("Index", "Customers");
        }
      
       

    }
}
