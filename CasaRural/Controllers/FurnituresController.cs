﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CasaRural.Data;
using CasaRural.Models;
using CasaRural.Filter;



namespace CasaRural.Controllers
{
    [ServiceFilter(typeof(SessionFilter))]
    public class FurnituresController : Controller
    {
        private readonly casaruralContext _context;

        public FurnituresController(casaruralContext context)
        {
            _context = context;
        }

        // GET: Furnitures
        public async Task<IActionResult> Index(String busqueda)
        {
            var furniture = _context.Furniture.Where(f => f.Active == true);
            if (!String.IsNullOrEmpty(busqueda))
            {
                furniture = _context.Furniture.Where(f => f.Name.Contains(busqueda));
            }
            return View(await furniture.ToListAsync());
        }

        // GET: Furnitures/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var furniture = await _context.Furniture
                .FirstOrDefaultAsync(m => m.ID == id);
            if (furniture == null)
            {
                return NotFound();
            }

            return View(furniture);
        }

        // GET: Furnitures/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Furnitures/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,BuyDate,High,Weight,Image,Available,Active")] Furniture furniture)
        {
            if (ModelState.IsValid)
            {
                furniture.Active = true;
                furniture.Available = true;
                _context.Add(furniture);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(furniture);
        }

        // GET: Furnitures/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var furniture = await _context.Furniture.FindAsync(id);
            if (furniture == null)
            {
                return NotFound();
            }
            return View(furniture);
        }

        // POST: Furnitures/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,BuyDate,High,Weight,Image,Available,Active")] Furniture furniture)
        {
            if (id != furniture.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(furniture);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FurnitureExists(furniture.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(furniture);
        }

        // GET: Furnitures/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var furniture = await _context.Furniture
                .FirstOrDefaultAsync(m => m.ID == id);
            if (furniture == null)
            {
                return NotFound();
            }
            
            return View(furniture);
        }

        // POST: Furnitures/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var furniture = await _context.Furniture.FindAsync(id);
            var furnitureInventory = _context.Inventory.Where(f => f.FurnitureID.Equals(id) && furniture.Available == false).FirstOrDefault();
            if (furnitureInventory != null)
            {
                TempData["ErrorMessage"] = "El mueble esta siendo usado y no puede eliminarse";
                return RedirectToAction(nameof(Delete));
            }
            _context.Furniture.Remove(furniture);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FurnitureExists(int id)
        {
            return _context.Furniture.Any(e => e.ID == id);
        }

        //Ver todo los registros existentes
        public async Task<IActionResult> VerTodoLosRegistros()
        {
            return View(await _context.Furniture.ToListAsync());
        }

            //Borrado lógico.
            public async Task<IActionResult> BorradoLogico(int? id)
        {

            var furniture = await _context.Furniture.FindAsync(id);

           
            
            var furnitureInventory = _context.Inventory.Where(f => f.FurnitureID.Equals(id) && furniture.Available == false).FirstOrDefault();
            if (furnitureInventory != null)
            {
                TempData["ErrorMessage"] = "El mueble esta siendo usado y no puede eliminarse";
                return RedirectToAction(nameof(Index));
            }
            furniture.Active = false;
            furniture.Available = false;
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }

      

    }
}
