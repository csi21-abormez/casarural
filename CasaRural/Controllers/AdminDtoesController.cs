﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using CasaRural.dtos;
using CasaRural.Data;
using CasaRural.Models;
using Microsoft.EntityFrameworkCore;
using System;
using Microsoft.AspNetCore.Http;
using CasaRural.Filter;



namespace CasaRural.Controllers
{

   
    public class AdminDtoesController : Controller
    {
        private readonly casaruralContext _context;

        public AdminDtoesController(casaruralContext context)
        {
            _context = context;
        }

       

       

        // GET: AdminDtoes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: AdminDtoes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,Surname,Phone,Username,Password,Email,Active")] AdminDto adminDto)
        {
            if (ModelState.IsValid)
            {
                var existeAdmin = _context.Admin.Include(u => u.UserAccount).Where(u => u.UserAccount.Username.Equals(adminDto.Username)).ToList();
                var existeCustomer = _context.Customer.Include(u => u.UserAccount).Where(u => u.UserAccount.Username.Equals(adminDto.Username)).ToList();
                var existeEmployed = _context.Employed.Include(U => U.UserAccount).Where(u => u.UserAccount.Username.Equals(adminDto.Username)).ToList();

                var roleID = _context.Role.Where(r => r.Name.Equals("admin")).First();

                //Creamos un UserAccount
                UserAccount userAccount = new UserAccount();
                userAccount.Username = adminDto.Username;
                userAccount.Password = Util.Encriptar(adminDto.Password);
                userAccount.Email = adminDto.Email;
                userAccount.Active = true;
                userAccount.RoleID = roleID.ID;
                if (existeAdmin.Count != 0 || existeCustomer.Count != 0 || existeEmployed.Count != 0)
                {
                    TempData["ErrorMessage"] = "Ya existe hay una cuenta con este nombre y contraseña.";
                    return RedirectToAction(nameof(Create));
                }
                _context.UserAccount.Add(userAccount);

                await _context.SaveChangesAsync();

                //Creamos un Admin
                Admin admin = new Admin();

                admin.Name = adminDto.Name;
                admin.Surname = adminDto.Surname;
                admin.Phone = adminDto.Phone;
                admin.UserAccountID = userAccount.ID;

               
               
                _context.Admin.Add(admin);
                await _context.SaveChangesAsync();


                if (HttpContext.Session.GetString("name") != null )
                {
                    return RedirectToAction("Index", "Admins");
                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }

               
               

            }
            return RedirectToAction("Index", "Admins");
        }

       
    }
}
