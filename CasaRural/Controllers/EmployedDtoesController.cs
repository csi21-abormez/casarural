﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using Microsoft.AspNetCore.Http;
using CasaRural.Data;
using CasaRural.Models;
using CasaRural.dtos;
using Microsoft.EntityFrameworkCore;
using CasaRural.Filter;


namespace CasaRural.Controllers
{
    public class EmployedDtoesController : Controller
    {
        private readonly casaruralContext _context;

        public EmployedDtoesController(casaruralContext context)
        {
            _context = context;
        }

        // GET: TeacherDtoes


        // GET: TeacherDtoes/Create
        public IActionResult Create()
        {
            ViewData["JobPositionsID"] = new SelectList(_context.JobPosition, "ID", "Name");
            return View();
        }

        // POST: TeacherDtoes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,Surname,Phone,Age,Username,Password,Email,Active,NameJobPosition,Salary")] EmployedDto employedDto)
        {
            if (ModelState.IsValid)
            {
                var jobPositionID = _context.JobPosition.Where(j => j.ID.Equals(employedDto.NameJobPosition)).First();
                var role = _context.Role.Where(r => r.Name.Equals(jobPositionID.Name)).First();
               
               
                //creamos los userAccount;
                UserAccount userAccount = new UserAccount();
                userAccount.Username = employedDto.Username;
                userAccount.Password = Util.Encriptar(employedDto.Password);
                userAccount.Email = employedDto.Email;
                userAccount.Active = true;
                userAccount.RoleID = role.ID;
                var existeAdmin = _context.Admin.Include(u => u.UserAccount).Where(u => u.UserAccount.Username.Equals(employedDto.Username)).ToList();
                var existeCustomer = _context.Customer.Include(u => u.UserAccount).Where(u => u.UserAccount.Username.Equals(employedDto.Username)).ToList();
                var existeEmployed = _context.Employed.Include(U => U.UserAccount).Where(u => u.UserAccount.Username.Equals(employedDto.Username)).ToList();
                if (existeAdmin.Count != 0 || existeCustomer.Count != 0 || existeEmployed.Count != 0)
                {
                    TempData["ErrorMessage"] = "Ya existe hay una cuenta con este nombre y contraseña.";
                    return RedirectToAction(nameof(Create));
                }

                _context.UserAccount.Add(userAccount);
                await _context.SaveChangesAsync();

                //creamos Teacher;

                Employed employed = new Employed();

                employed.Name = employedDto.Name;
                employed.Surname = employedDto.Surname;
                employed.Phone = employedDto.Phone;
                employed.Age = employedDto.Age;
                employed.Salary = employedDto.Salary;
                employed.JobPostionID = employedDto.NameJobPosition;
                employed.UserAccountID = userAccount.ID;

                

                _context.Employed.Add(employed);
                await _context.SaveChangesAsync();
                if (HttpContext.Session.GetString("name") != null)
                {
                    return RedirectToAction("Index", "Employeds");
                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
                /*
                                var roleAdminID = _context.Role.Where(r => r.Name.Equals("Admin")).First();
                                var userAccountlist = _context.UserAccount.Where(u => u.RoleID.Equals(roleAdminID.ID)).ToList();
                                foreach (UserAccount u in userAccountlist)
                                {
                                    Util.SendEmail(u.Email, teacherDto.Name, "Prueba de email de creacion de teacher");

                                }
                */
              
            }
            return View(employedDto);
        }
    }
}
