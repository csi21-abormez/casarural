﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CasaRural.Data;
using CasaRural.Models;
using CasaRural.Filter;

namespace CasaRural.Controllers
{
    [ServiceFilter(typeof(SessionFilter))]
    public class SupplementsController : Controller
    {
        private readonly casaruralContext _context;

        public SupplementsController(casaruralContext context)
        {
            _context = context;
        }

        // GET: Supplements
        public async Task<IActionResult> Index()
        {
            var casaruralContext = _context.Supplement.Include(s => s.Booking).Include(c => c.Booking.Customer).Include(s => s.Other).Where(s => s.Active == true);
            return View(await casaruralContext.ToListAsync());
        }

        // GET: Supplements/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var supplement = await _context.Supplement
                .Include(s => s.Booking)
                .Include(s => s.Other)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (supplement == null)
            {
                return NotFound();
            }

            return View(supplement);
        }

        // GET: Supplements/Create
        public IActionResult Create()
        {
            List<Booking> booking = _context.Booking.ToList();
            List<Booking> bookingId = new List<Booking>();
            foreach(Booking fecha in booking)
            {
                if (DateTime.Now < fecha.BookingEndDate)
                {

                    bookingId.Add(fecha);
                    
                }
               
            }
            ViewData["BookingID"] = new SelectList(bookingId, "ID", "ID");
            ViewData["OtherID"] = new SelectList(_context.Other, "ID", "Name");
            return View();

        }

        // POST: Supplements/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,OtherID,BookingID,Active")] Supplement supplement)
        {
            if (ModelState.IsValid)
            {
                supplement.Active = true;
                _context.Add(supplement);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["BookingID"] = new SelectList(_context.Booking, "ID", "ID", supplement.BookingID);
            ViewData["OtherID"] = new SelectList(_context.Other, "ID", "Name", supplement.OtherID);
            return View(supplement);
        }

        // GET: Supplements/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var supplement = await _context.Supplement.FindAsync(id);
            if (supplement == null)
            {
                return NotFound();
            }
            ViewData["BookingID"] = new SelectList(_context.Booking, "ID", "ID", supplement.BookingID);
            ViewData["OtherID"] = new SelectList(_context.Other, "ID", "Name", supplement.OtherID);
            return View(supplement);
        }

        // POST: Supplements/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,OtherID,BookingID,Active")] Supplement supplement)
        {
            if (id != supplement.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(supplement);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SupplementExists(supplement.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["BookingID"] = new SelectList(_context.Booking, "ID", "ID", supplement.BookingID);
            
            ViewData["OtherID"] = new SelectList(_context.Other, "ID", "Name", supplement.OtherID);
            return View(supplement);
        }

        // GET: Supplements/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var supplement = await _context.Supplement
                .Include(s => s.Booking)
                .Include(s => s.Other)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (supplement == null)
            {
                return NotFound();
            }

            return View(supplement);
        }

        // POST: Supplements/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var supplement = await _context.Supplement.FindAsync(id);
            _context.Supplement.Remove(supplement);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SupplementExists(int id)
        {
            return _context.Supplement.Any(e => e.ID == id);
        }

        //Ver todo los registros existentes
        public async Task<IActionResult> VerTodoLosRegistros()
        {
            var casaruralContext = _context.Supplement.Include(s => s.Booking).Include(c => c.Booking.Customer).Include(s => s.Other);
            return View(await casaruralContext.ToListAsync());
        }

        //Borrado lógico.
        public async Task<IActionResult> BorradoLogico(int? id)
        {

            var supplement = await _context.Supplement.FindAsync(id);

            supplement.Active = false;

            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }
    }
}
