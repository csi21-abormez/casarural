﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CasaRural.Data;
using CasaRural.Models;
using CasaRural.Filter;

namespace CasaRural.Controllers
{
    [ServiceFilter(typeof(SessionFilter))]
    public class AdminsController : Controller
    {
        private readonly casaruralContext _context;

        public AdminsController(casaruralContext context)
        {
            _context = context;
        }

        // GET: Admins
        public async Task<IActionResult> Index(String busqueda)
        {
            var casaruralContext = _context.Admin.Include(a => a.UserAccount).OrderBy(u => u.Surname).Where(a => a.UserAccount.Active == true);
            if (!String.IsNullOrEmpty(busqueda))
            {
                casaruralContext = _context.Admin.Where(f => f.Name.Contains(busqueda));
            }
            return View(await casaruralContext.ToListAsync());
        }

        // GET: Admins/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var admin = await _context.Admin
                .Include(a => a.UserAccount)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (admin == null)
            {
                return NotFound();
            }

            return View(admin);
        }

        // GET: Admins/Create
        public IActionResult Create()
        {
            ViewData["UserAccountID"] = new SelectList(_context.UserAccount, "ID", "Username");
            return View();
        }

        // POST: Admins/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,Surname,Phone,UserAccountID")] Admin admin)
        {
            if (ModelState.IsValid)
            {
                _context.Add(admin);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserAccountID"] = new SelectList(_context.UserAccount, "ID", "Username", admin.UserAccountID);
            return View(admin);
        }

        // GET: Admins/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var admin = await _context.Admin.FindAsync(id);
            if (admin == null)
            {
                return NotFound();
            }
            ViewData["UserAccountID"] = new SelectList(_context.UserAccount, "ID", "Username", admin.UserAccountID);
            return View(admin);
        }

        // POST: Admins/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,Surname,Phone,UserAccountID")] Admin admin)
        {
            if (id != admin.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(admin);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AdminExists(admin.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserAccountID"] = new SelectList(_context.UserAccount, "ID", "Username", admin.UserAccountID);
            return View(admin);
        }

        // GET: Admins/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var admin = await _context.Admin
                .Include(a => a.UserAccount)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (admin == null)
            {
                return NotFound();
            }

            return View(admin);
        }

        // POST: Admins/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var admin = await _context.Admin.FindAsync(id);
            _context.Admin.Remove(admin);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AdminExists(int id)
        {
            return _context.Admin.Any(e => e.ID == id);
        }

        // GET: UserAccounts
        public async Task<IActionResult> VerTodoLosRegistros()
        {
            var casaruralContext = _context.Admin.Include(a => a.UserAccount).OrderBy(u => u.Surname);
            return View(await casaruralContext.ToListAsync());
        }

        //Borrado lógico.
        public async Task<IActionResult> BorradoLogico(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var admin = await _context.Admin.FindAsync(id);
            var useraccount = _context.UserAccount.Where(u => u.ID.Equals(admin.UserAccountID)).FirstOrDefault();

            useraccount.Active = false;
            
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }
    }
}
