﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CasaRural.Data;
using CasaRural.Models;
using Microsoft.AspNetCore.Http;
using CasaRural.Filter;


namespace CasaRural.Controllers
{
    [ServiceFilter(typeof(SessionFilter))]
    public class RoomCleaningsController : Controller
    {
        private readonly casaruralContext _context;

        public RoomCleaningsController(casaruralContext context)
        {
            _context = context;
        }

        // GET: RoomCleanings
        public async Task<IActionResult> Index(String busqueda)
        {
            var casaruralContext = _context.RoomCleaning.Include(r => r.Employed).Include(r => r.Room).Where(rc => rc.Active == true).OrderBy(r => r.CleanDate);
            var jobPosition = _context.JobPosition.Where(j => j.Name.Equals("Limpiador")).FirstOrDefault();
            var employed = _context.Employed.Include(u => u.UserAccount).Where(u => u.JobPostionID.Equals(jobPosition.ID) && u.UserAccountID.ToString().Equals(HttpContext.Session.GetString("id"))).FirstOrDefault();
            if(employed != null)
            {
                casaruralContext = _context.RoomCleaning.Include(r => r.Employed).Include(r => r.Room).Where(f => f.EmployedID.Equals(employed.ID)).OrderBy(r => r.CleanDate);
            }
            
            if (busqueda != null)
            {

                casaruralContext = _context.RoomCleaning.Include(r => r.Employed).Include(r => r.Room).Where(f => f.CleanDate >= DateTime.Parse(busqueda) && f.Clean == true).OrderBy(r => r.CleanDate);
            }

           
           
            
            return View(await casaruralContext.ToListAsync());
        }

        // GET: RoomCleanings/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var roomCleaning = await _context.RoomCleaning
                .Include(r => r.Employed)
                .Include(r => r.Room)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (roomCleaning == null)
            {
                return NotFound();
            }

            return View(roomCleaning);
        }

        // GET: RoomCleanings/Create
        public IActionResult Create()
        {
            ViewData["EmployedID"] = new SelectList(_context.Employed, "ID", "Name");
            ViewData["RoomID"] = new SelectList(_context.Room, "ID", "Name");
            return View();
        }

        // POST: RoomCleanings/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,CleanDate,Observations,Clean,EmployedID,RoomID,Active")] RoomCleaning roomCleaning)
        {
            if (ModelState.IsValid)
            {
                var employed = _context.Employed.Where(e => e.ID.Equals(roomCleaning.EmployedID)).FirstOrDefault();
                var jobPosition = _context.JobPosition.Where(j => j.ID.Equals(employed.JobPostionID)).FirstOrDefault();
                if (!jobPosition.Name.Equals("Limpiador"))
                {
                    TempData["ErrorMessage"] = "El empleado que intenta asignar no es un limpiador.";
                    return RedirectToAction(nameof(Create));
                }
                roomCleaning.Active = true;
                _context.Add(roomCleaning);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["EmployedID"] = new SelectList(_context.Employed, "ID", "Name", roomCleaning.EmployedID);
            ViewData["RoomID"] = new SelectList(_context.Room, "ID", "Name", roomCleaning.RoomID);
            return View(roomCleaning);
        }

        // GET: RoomCleanings/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var roomCleaning = await _context.RoomCleaning.FindAsync(id);
            if (roomCleaning == null)
            {
                return NotFound();
            }
            ViewData["EmployedID"] = new SelectList(_context.Employed, "ID", "Name", roomCleaning.EmployedID);
            ViewData["RoomID"] = new SelectList(_context.Room, "ID", "Name", roomCleaning.RoomID);
            return View(roomCleaning);
        }

        // POST: RoomCleanings/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,CleanDate,Observations,Clean,EmployedID,RoomID,Active")] RoomCleaning roomCleaning)
        {
            if (id != roomCleaning.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(roomCleaning);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RoomCleaningExists(roomCleaning.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["EmployedID"] = new SelectList(_context.Employed, "ID", "Name", roomCleaning.EmployedID);
            ViewData["RoomID"] = new SelectList(_context.Room, "ID", "Name", roomCleaning.RoomID);
            return View(roomCleaning);
        }

        // GET: RoomCleanings/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var roomCleaning = await _context.RoomCleaning
                .Include(r => r.Employed)
                .Include(r => r.Room)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (roomCleaning == null)
            {
                return NotFound();
            }

            return View(roomCleaning);
        }

        // POST: RoomCleanings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var roomCleaning = await _context.RoomCleaning.FindAsync(id);
            _context.RoomCleaning.Remove(roomCleaning);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RoomCleaningExists(int id)
        {
            return _context.RoomCleaning.Any(e => e.ID == id);
        }

        //Ver todo los registros existentes
        public async Task<IActionResult> VerTodoLosRegistros()
        {
            var casaruralContext = _context.RoomCleaning.Include(r => r.Employed).Include(r => r.Room);
            return View(await casaruralContext.ToListAsync());
        }

        //Borrado lógico.
        public async Task<IActionResult> BorradoLogico(int? id)
        {

            var roomCleaning = await _context.RoomCleaning.FindAsync(id);

            roomCleaning.Active = false;

            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }
    }
}
