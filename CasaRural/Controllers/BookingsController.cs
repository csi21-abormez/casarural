﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CasaRural.Data;
using CasaRural.Models;
using CasaRural.Filter;


namespace CasaRural.Controllers
{
    [ServiceFilter(typeof(SessionFilter))]
    public class BookingsController : Controller
    {
        private readonly casaruralContext _context;

        public BookingsController(casaruralContext context)
        {
            _context = context;
        }

        // GET: Bookings
        public async Task<IActionResult> Index(String busqueda )
        {
            var casaruralContext = _context.Booking.Include(b => b.Customer).Include(b => b.Diet).Include(b => b.Room).Where(b => b.Active == true).OrderBy(b => b.BookingStartDate);
            try
            {
                if (/*!String.IsNullOrEmpty(busqueda)*/ busqueda != null)
                {

                    casaruralContext = _context.Booking.Include(b => b.Customer).Include(b => b.Diet).Include(b => b.Room).Where(f => f.BookingStartDate >= DateTime.Parse(busqueda)).OrderBy(b => b.BookingEndDate);
                }
            }
            catch(FormatException e)
            {
                TempData["ErrorMessage"] = "El formato de fecha no es correcto.";
                return RedirectToAction(nameof(Index));
            }
            return View(await casaruralContext.ToListAsync());
        }

        // GET: Bookings/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booking = await _context.Booking
                .Include(b => b.Customer)
                .Include(b => b.Diet)
                .Include(b => b.Room)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (booking == null)
            {
                return NotFound();
            }

            return View(booking);
        }

        // GET: Bookings/Create
        public IActionResult Create()
        {
            ViewData["CustomerID"] = new SelectList(_context.Customer, "ID", "Name");
            ViewData["DietID"] = new SelectList(_context.Diet, "ID", "Name");
            ViewData["RoomID"] = new SelectList(_context.Room, "ID", "Name");

            return View();
        }

        // POST: Bookings/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,BookingStartDate,BookingEndDate,CustomerID,RoomID,DietID,Active")] Booking booking)
        {
            if (ModelState.IsValid)
            {
                //Comprobación de si existe o no una reserva para las fechas introducida. 
               var habitacionID = _context.Room.Where(r => r.ID.Equals(booking.RoomID)).FirstOrDefault();
                var reserva = _context.Booking.Where(b => (booking.BookingStartDate <= b.BookingStartDate || booking.BookingStartDate >= b.BookingStartDate) && (booking.BookingEndDate <= b.BookingEndDate) && booking.RoomID.Equals(b.RoomID)).ToList();

                if(reserva.Count != 0)
                {
                    TempData["ErrorMessage"] = "Ya hay una reserva de esta habitación para las fechas elegidas.";
                    return RedirectToAction(nameof(Create));
                }
                //Comprobación de habitación limpiada 
                var habitacionLimpia = _context.RoomCleaning.Where(rc => rc.RoomID.Equals(habitacionID.ID)
               && rc.Clean == false).FirstOrDefault();
                if(habitacionLimpia != null)
                {
                    TempData["ErrorMessage"] = "La habitación esta siendo limpiada y hasta que no se termine no se puede reservar.";
                    return RedirectToAction(nameof(Create));
                }
                booking.Active = true;
                _context.Add(booking);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CustomerID"] = new SelectList(_context.Customer, "ID", "Name", booking.CustomerID);
            ViewData["DietID"] = new SelectList(_context.Diet, "ID", "Name", booking.DietID);
            ViewData["RoomID"] = new SelectList(_context.Room, "ID", "Name", booking.RoomID);
            return View(booking);
        }

        // GET: Bookings/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booking = await _context.Booking.FindAsync(id);
            if (booking == null)
            {
                return NotFound();
            }
            ViewData["CustomerID"] = new SelectList(_context.Customer, "ID", "Name", booking.CustomerID);
            ViewData["DietID"] = new SelectList(_context.Diet, "ID", "Name", booking.DietID);
            ViewData["RoomID"] = new SelectList(_context.Room, "ID", "Name", booking.RoomID);
            return View(booking);
        }

        // POST: Bookings/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,BookingStartDate,BookingEndDate,CustomerID,RoomID,DietID,Active")] Booking booking)
        {
            if (id != booking.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(booking);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookingExists(booking.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CustomerID"] = new SelectList(_context.Customer, "ID", "Name", booking.CustomerID);
            ViewData["DietID"] = new SelectList(_context.Diet, "ID", "Name", booking.DietID);
            ViewData["RoomID"] = new SelectList(_context.Room, "ID", "Name", booking.RoomID);
            return View(booking);
        }

        // GET: Bookings/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booking = await _context.Booking
                .Include(b => b.Customer)
                .Include(b => b.Diet)
                .Include(b => b.Room)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (booking == null)
            {
                return NotFound();
            }

            return View(booking);
        }

        // POST: Bookings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var booking = await _context.Booking.FindAsync(id);
            _context.Booking.Remove(booking);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BookingExists(int id)
        {
            return _context.Booking.Any(e => e.ID == id);
        }
        // Calcula precio reserva dependienddo de los dias reservados, habitación, dieta y suplementos que haya consumido.
        public async Task<IActionResult> CalculoPrecio(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }

            var booking = await _context.Booking
                .Include(b => b.Customer)
                .Include(b => b.Diet)
                .Include(b => b.Room)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (booking == null)
            {
                return NotFound();
            }

            Booking bookings = _context.Booking.Where(b => b.ID == id).FirstOrDefault();
            Diet dieta = _context.Diet.Where(d => d.ID == bookings.DietID).FirstOrDefault();
            Double precioDieta = _context.Diet.Where(d => d.Price == dieta.Price).FirstOrDefault().Price;
            Room room = _context.Room.Where(r => r.ID == bookings.RoomID).FirstOrDefault();
            TypeRoom typeRoom = _context.TypeRoom.Where(y => y.ID == room.TypeRoomID).FirstOrDefault();
            Double precioRoom = _context.TypeRoom.Where(t => t.Price == typeRoom.Price).FirstOrDefault().Price;
            List<Supplement> lsupplement = _context.Supplement.Include(o => o.Other).Where(s => s.BookingID == bookings.ID).ToList();
            List<Double> other = new List<double>();
            foreach(Supplement supplement1 in lsupplement)
            {
                other.Add(supplement1.Other.Price);
               
            }
            
            var diasReservados =(Math.Abs( booking.BookingEndDate.DayOfYear - booking.BookingStartDate.DayOfYear));
            var resultado =  Math.Abs((((diasReservados * precioRoom) + precioDieta) * 1.21 ));
            
            foreach(Double x in other)
            {
                resultado += Math.Abs(x * 1.10);
                
            }
            ViewBag.fechaActual = DateTime.Now;
            ViewBag.diasReservados = diasReservados;
            ViewBag.precio = resultado.ToString("0.#");
            ViewBag.other = other;
            ViewBag.supplement = lsupplement;
            return View(booking);

        }

        public async Task<IActionResult> VerTodoLosRegistros()
        {
            var casaruralContext = _context.Booking.Include(b => b.Customer).Include(b => b.Diet).Include(b => b.Room);
            return View(await casaruralContext.ToListAsync());
        }

        //Borrado lógico.
        public async Task<IActionResult> BorradoLogico(int? id)
        {

            var booking = await _context.Booking.FindAsync(id);

            booking.Active = false;

            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }
    }
}
