﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CasaRural.Data;
using CasaRural.Models;
using CasaRural.Filter;


namespace CasaRural.Controllers
{
    [ServiceFilter(typeof(SessionFilter))]
    public class InventoriesController : Controller
    {
        private readonly casaruralContext _context;

        public InventoriesController(casaruralContext context)
        {
            _context = context;
        }

        // GET: Inventories
        public async Task<IActionResult> Index()
        {
            var casaruralContext = _context.Inventory.Include(i => i.Furniture).Include(i => i.Room).Where(i => i.Active == true);
            
            return View(await casaruralContext.ToListAsync());
        }

        // GET: Inventories/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inventory = await _context.Inventory
                .Include(i => i.Furniture)
                .Include(i => i.Room)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (inventory == null)
            {
                return NotFound();
            }

            return View(inventory);
        }

        // GET: Inventories/Create
        public IActionResult Create()
        {
            //Recuperamos la lista de muebles que esten activos
            ViewData["FurnitureID"] = new SelectList(_context.Furniture.Where(f => f.Available == true && f.Active == true), "ID", "Name");
            ViewData["RoomID"] = new SelectList(_context.Room, "ID", "Name");
            return View();
        }

        // POST: Inventories/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,FurnitureID,RoomID,Active")] Inventory inventory)
        {
            if (ModelState.IsValid)
            {
                inventory.Active = true;
                var furniture = _context.Furniture.Where(f => f.ID.Equals(inventory.FurnitureID)).FirstOrDefault();
                furniture.Available = false;
                _context.Add(inventory);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["FurnitureID"] = new SelectList(_context.Furniture, "ID", "Name", inventory.FurnitureID);
            ViewData["RoomID"] = new SelectList(_context.Room, "ID", "Name", inventory.RoomID);
            return View(inventory);
        }

        // GET: Inventories/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inventory = await _context.Inventory.FindAsync(id);
            if (inventory == null)
            {
                return NotFound();
            }
            ViewData["FurnitureID"] = new SelectList(_context.Furniture, "ID", "Name", inventory.FurnitureID);
            ViewData["RoomID"] = new SelectList(_context.Room, "ID", "Name", inventory.RoomID);
            return View(inventory);
        }

        // POST: Inventories/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,FurnitureID,RoomID,Active")] Inventory inventory)
        {
            if (id != inventory.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                   
                    _context.Update(inventory);
                    var furniture = _context.Furniture.Where(f => f.ID.Equals(inventory.FurnitureID)).FirstOrDefault();
                    furniture.Available = false;
                    furniture.Active = true;
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!InventoryExists(inventory.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["FurnitureID"] = new SelectList(_context.Furniture, "ID", "Name", inventory.FurnitureID);
            ViewData["RoomID"] = new SelectList(_context.Room, "ID", "Name", inventory.RoomID);
            return View(inventory);
        }

        // GET: Inventories/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inventory = await _context.Inventory
                .Include(i => i.Furniture)
                .Include(i => i.Room)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (inventory == null)
            {
                return NotFound();
            }

            return View(inventory);
        }

        // POST: Inventories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var inventory = await _context.Inventory.FindAsync(id);
            _context.Inventory.Remove(inventory);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool InventoryExists(int id)
        {
            return _context.Inventory.Any(e => e.ID == id);
        }

        //Ver todo los registros existentes
        public async Task<IActionResult> VerTodoLosRegistros()
        {
            var casaruralContext = _context.Inventory.Include(i => i.Furniture).Include(i => i.Room);
            return View(await casaruralContext.ToListAsync());
        }

        //Borrado lógico.
        public async Task<IActionResult> BorradoLogico(int? id)
        {

            var inventory = await _context.Inventory.FindAsync(id);

            inventory.Active = false;
            var furniture = _context.Furniture.Where(f => f.ID.Equals(inventory.FurnitureID)).FirstOrDefault();
            furniture.Available = true;

            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }
    }
}
