﻿using System;
using CasaRural.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

#nullable disable

namespace CasaRural.Data
{
    public partial class casaruralContext : DbContext
    {
        public casaruralContext()
        {
        }

        public casaruralContext(DbContextOptions<casaruralContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // Cargar en la variable configuration los datos del JSON
            IConfiguration configuration = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json")
.Build();
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Modern_Spanish_CI_AS");

            OnModelCreatingPartial(modelBuilder);

            //Carga inicial de Roles 

            modelBuilder.Entity<Role>().HasData(
               new Role

               {
                   ID = 1,
                   Name = "Admin",
                   Description = "Rol de Admin",
                   Active = true                   

               },
             
                 new Role

                 {
                     ID = 2,
                     Name = "Customer",
                     Description = "Rol de Customer",
                     Active = true

                 },
                    new Role

                    {
                        ID = 3,
                        Name = "Director",
                        Description = "Rol de Employed",
                        Active = true
                    },
                      new Role

                      {
                          ID = 4,
                          Name = "Subdirector",
                          Description = "Rol de Encargado de la subdirección de la empresa",
                          Active = true
                      },
                       new Role

                       {
                           ID = 5,
                           Name = "Jefe Cocina",
                           Description = "Encargado de la elaboración de las dietas",
                           Active = true
                       },
                      new Role

                      {
                          ID = 6,
                          Name = "Administracion",
                          Description = "Encargado de la administración de la empresa",
                          Active = true
                      },
                        new Role

                        {
                            ID = 7,
                            Name = "Jefe de Mantenimiento",
                            Description = "Encargado de la elaboración de las dietas",
                            Active = true
                        },
                        new Role

                        {
                            ID = 8,
                            Name = "Recepcionista",
                            Description = "Encargado de la recepción de la casa rural",
                            Active = true
                        },
                      new Role

                      {
                          ID = 9,
                          Name = "Gobernante",
                          Description = "Encargado del correcto orden y limpieza de la casa rural",
                          Active = true
                      },
                          new Role

                          {
                              ID = 10,
                              Name = "Limpiador",
                              Description = "Encargado de la correcta limpieza de las habitaciones",
                              Active = true
                          });

            //Carga inicial de Menus

            modelBuilder.Entity<Menu>().HasData(
               new Menu

               {
                   ID = 1,
                   Controller = "Admins",
                   Action = "Index",
                   Label = "Admin",
                   Active = true
               },
                new Menu
                {
                    ID = 2,
                    Controller = "Bookings",
                    Action = "Index",
                    Label = "Booking",
                    Active = true
                },

                 new Menu

                 {
                     ID = 3,
                     Controller = "Customers",
                     Action = "Index",
                     Label = "Customer",
                     Active = true
                 },
                new Menu
                {
                    ID = 4,
                    Controller = "Diets",
                    Action = "Index",
                    Label = "Diet",
                    Active = true
                },
                 new Menu

                 {
                     ID = 5,
                     Controller = "Employeds",
                     Action = "Index",
                     Label = "Employed",
                     Active = true
                 },
                new Menu
                {
                    ID = 6,
                    Controller = "Furnitures",
                    Action = "Index",
                    Label = "Furniture",
                    Active = true
                },
                 new Menu

                 {
                     ID = 7,
                     Controller = "Inventories",
                     Action = "Index",
                     Label = "Inventory",
                     Active = true
                 },
                new Menu
                {
                    ID = 8,
                    Controller = "JobPositions",
                    Action = "Index",
                    Label = "JobPosition",
                    Active = true
                },
                
                new Menu
                {
                    ID = 9,
                    Controller = "RoomCleanings",
                    Action = "Index",
                    Label = "RoomCleaning",
                    Active = true
                },
                 new Menu

                 {
                     ID = 10,
                     Controller = "Rooms",
                     Action = "Index",
                     Label = "Room",
                     Active = true
                 },
                new Menu
                {
                    ID = 11,
                    Controller = "Supplements",
                    Action = "Index",
                    Label = "Supplement",
                    Active = true
                },
                 new Menu

                 {
                     ID = 12,
                     Controller = "TypeRooms",
                     Action = "Index",
                     Label = "TypeRoom",
                     Active = true
                 },
               
                 new Menu

                 {
                     ID = 13,
                     Controller = "Other",
                     Action = "Index",
                     Label = "Other",
                     Active = true
                 });

            //Carga inicial RoleHasMenu

            modelBuilder.Entity<RoleHasMenu>().HasData(
              new RoleHasMenu

              {
                  ID = 1,
                  MenuID = 1,
                  RoleID = 1,
                  Active = true
              },
              new RoleHasMenu

              {
                  ID = 2,
                  MenuID = 2,
                  RoleID = 1,
                  Active = true
              },
              new RoleHasMenu

              {
                  ID = 3,
                  MenuID = 3,
                  RoleID = 1,
                  Active = true
              },
              new RoleHasMenu

              {
                  ID = 4,
                  MenuID = 4,
                  RoleID = 1,
                  Active = true
              },
              new RoleHasMenu

              {
                  ID = 5,
                  MenuID = 5,
                  RoleID = 1,
                  Active = true
              },
              new RoleHasMenu

              {
                  ID = 6,
                  MenuID = 6,
                  RoleID = 1,
                  Active = true
              },
              new RoleHasMenu

              {
                  ID = 7,
                  MenuID = 7,
                  RoleID = 1,
                  Active = true
              },
              new RoleHasMenu

              {
                  ID = 8,
                  MenuID = 8,
                  RoleID = 1,
                  Active = true
              },
              new RoleHasMenu

              {
                  ID = 9,
                  MenuID = 9,
                  RoleID = 1,
                  Active = true
              },
              new RoleHasMenu

              {
                  ID = 10,
                  MenuID = 10,
                  RoleID = 1,
                  Active = true
              },
              new RoleHasMenu

              {
                  ID = 11,
                  MenuID = 11,
                  RoleID = 1,
                  Active = true
              },
              new RoleHasMenu

              {
                  ID = 12,
                  MenuID = 12,
                  RoleID = 1,
                  Active = true
              },
              new RoleHasMenu

              {
                  ID = 13,
                  MenuID = 13,
                  RoleID = 1,
                  Active = true
              },
              new RoleHasMenu

              {
                  ID = 14,
                  MenuID = 14,
                  RoleID = 1,
                  Active = true
              },
              new RoleHasMenu

              {
                  ID = 15,
                  MenuID = 15,
                  RoleID = 1,
                  Active = true
              },
              new RoleHasMenu

              {
                  ID = 16,
                  MenuID = 16,
                  RoleID = 1,
                  Active = true
              },
              new RoleHasMenu

              {
                  ID = 17,
                  MenuID = 17,
                  RoleID = 1,
                  Active = true
              });

            //Carga inicial de userAccount
            modelBuilder.Entity<UserAccount>().HasData(
                new UserAccount
                {
                    ID = 1,
                    Username = "admin",
                    Password = Util.Encriptar("Altair123$%"),
                    Email = "admin@email.es",
                    Active = true,
                    RoleID = 1
                },

                  new UserAccount
                  {
                      ID = 2,
                      Username = "customer",
                      Password = Util.Encriptar("1234"),
                      Email = "customer@email.es",
                      Active = true,
                      RoleID = 2
                  },
                 new UserAccount
                 {
                     ID = 3,
                     Username = "Director",
                     Password = Util.Encriptar("1234"),
                     Email = "Director@email.es",
                     Active = true,
                     RoleID = 3
                 },
                  new UserAccount
                  {
                      ID = 4,
                      Username = "admin2",
                      Password = Util.Encriptar("Altair123$%"),
                      Email = "admin@email.es",
                      Active = false,
                      RoleID = 1
                  }
                );

            //Carga inicial de Dietas
            modelBuilder.Entity<Diet>().HasData(
                new Diet
                {
                    ID = 1,
                    Name = "Full board",
                    Price = 50,
                    Active = true
                },
                 new Diet
                 {
                     ID = 2,
                     Name = "Hall board",
                     Price = 30,
                     Active = true
                 },
                  new Diet
                  {
                      ID = 3,
                      Name = "only breakfast",
                      Price = 15,
                      Active = true
                  },
                   new Diet
                   {
                       ID = 4,
                       Name = "accommodation only",
                       Price = 0,
                       Active = true
                   });
            //Carga inicial de puestos de trabajo
            modelBuilder.Entity<JobPosition>().HasData(
               new JobPosition
               {
                   ID = 1,
                   Name = "Director",
                   Description = "Encargado de la dirección de la empresa",
                    Active = true
               },
                new JobPosition
                {
                    ID = 2,
                    Name = "Subdirector",
                    Description = "Encargado de la subdirección de la empresa",
                     Active = true
                },
                 new JobPosition
                 {
                     ID = 3,
                     Name = "Jefe Cocina",
                     Description = "Encargado de la elaboración de las dietas",
                     Active = true
                 },
                 new JobPosition
                 {
                     ID = 4,
                     Name = "Administración",
                     Description = "Encargado de la administración de la empresa",
                      Active = true
                 },
                  new JobPosition
                  {
                      ID = 5,
                      Name = "Jefe de Mantenimiento",
                      Description = "Encargado del mantenimiento de la casa rural",
                       Active = true
                  },
                   new JobPosition
                   {
                       ID = 6,
                       Name = "Recepcionista",
                       Description = "Encargado de la recepción de la casa rural",
                       Active = true
                   },
                    new JobPosition
                    {
                        ID = 7,
                        Name = "Gobernante",
                        Description = "Encargado del correcto orden y limpieza de la casa rural",
                        Active = true
                    },
                    new JobPosition
                    {
                        ID = 8,
                        Name = "Limpiador",
                        Description = "Encargado de la correcta limpieza de las habitaciones",
                        Active = true
                    });

            //Carga inicial de otros

            
            modelBuilder.Entity<Other>().HasData(
                new Other
                {
                    ID = 1,
                    Name = "Consumición agua minibar",
                    Price = 1.50,
                    Active = true,
                },
                new Other
                {
                    ID = 2,
                    Name = "Consumición refresco minibar",
                    Price = 2.50,
                    Active = true,
                },
                 new Other
                 {
                     ID = 3,
                     Name = "Consumición alcohol minibar",
                     Price = 3.50 ,
                     Active = true,
                 },
                  new Other
                  {
                      ID = 4,
                      Name = "Consumición alcohol premium minibar",
                      Price = 5.50,
                      Active = true,
                  });

            //Carca inicial de tipos de habitaciones 

            modelBuilder.Entity<TypeRoom>().HasData(
               new TypeRoom
               {
                   ID = 1,
                   Name = "Suite",
                   Description = "Suite",
                   Price = 95,
                   Active = true

               },
                 new TypeRoom
                 {
                     ID = 2,
                     Name = "Double",
                     Description = "Habitación de matrimonio",
                     Price = 60,
                     Active = true

                 },
                   new TypeRoom
                   {
                       ID = 3,
                       Name = "Singgle",
                       Description = "Habitación simple para una persona",
                       Price = 45,
                       Active = true

                   });

            //Carga inicial de admin
            modelBuilder.Entity<Admin>().HasData(
               new Admin
               {

                   ID = 1,
                   Name = "admin",
                   Surname = "admin",
                   Phone = "655555555",
                   UserAccountID = 1
               },
               new Admin
               {

                   ID = 2,
                   Name = "admin2",
                   Surname = "admin2",
                   Phone = "655555555",
                   UserAccountID = 4
               }
               );

            //Carga inicial de Customer
            modelBuilder.Entity<Customer>().HasData(
               new Customer
               {

                   ID = 1,
                   Name = "customer",
                   Surname = "customer",
                   Phone = "655555555",
                   UserAccountID = 3,
                   PostalCode = "41111",
                   Address = "C/prueba",
                   City = "Sevilla",
                   DNI = "00000000L",
 
               });


            //Carga inicial de Employed
            modelBuilder.Entity<Employed>().HasData(
               new Employed
               {

                   ID = 1,
                   Name = "Director",
                   Surname = "employed",
                   Phone = "655555555",
                   UserAccountID = 2,
                   Age = 30,
                   Salary = 50000,
                   JobPostionID = 1

               });



        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

        public DbSet<CasaRural.Models.Menu> Menu { get; set; }

        public DbSet<CasaRural.Models.Booking> Booking { get; set; }

        public DbSet<CasaRural.Models.Customer> Customer { get; set; }

        public DbSet<CasaRural.Models.Diet> Diet { get; set; }

        public DbSet<CasaRural.Models.Employed> Employed { get; set; }

        public DbSet<CasaRural.Models.Furniture> Furniture { get; set; }

        public DbSet<CasaRural.Models.Inventory> Inventory { get; set; }

        public DbSet<CasaRural.Models.JobPosition> JobPosition { get; set; }

        public DbSet<CasaRural.Models.Other> Other { get; set; }

        public DbSet<CasaRural.Models.Role> Role { get; set; }

        public DbSet<CasaRural.Models.RoleHasMenu> RoleHasMenu { get; set; }

        public DbSet<CasaRural.Models.Room> Room { get; set; }

        public DbSet<CasaRural.Models.RoomCleaning> RoomCleaning { get; set; }

        public DbSet<CasaRural.Models.Supplement> Supplement { get; set; }

        public DbSet<CasaRural.Models.TypeRoom> TypeRoom { get; set; }

        public DbSet<CasaRural.Models.UserAccount> UserAccount { get; set; }

        public DbSet<CasaRural.Models.Admin> Admin { get; set; }
    }
}
