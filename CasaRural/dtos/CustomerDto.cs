﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CasaRural.dtos
{
  
    public class CustomerDto
    {
        public int ID { get; set; }
        [Display(Name = "Nombre")]
        [Required]
        [StringLength(20, ErrorMessage = "the fiel {0} can´t be longer than 20 ")]
        public string Name { get; set; }

        [Required]
        [StringLength(80, ErrorMessage = "the fiel {0} can´t be longer than 80 ")]
        public string Surname { get; set; }
        [Required]
        [Phone(ErrorMessage = "the fiel {0} is not valid ")]
        public string Phone { get; set; }

        public DateTime Birthday { get; set; }

        public string DNI { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string postalCode { get; set; }

       

        [Required(ErrorMessage = "it can´t be empty")]
        [StringLength(10, MinimumLength = 5, ErrorMessage = "the number of characters must be between 5 and 10")]
        public string Username { get; set; }

        [Required(ErrorMessage = "it can´t be empty")]

        [DataType(DataType.Password)]

        public string Password { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(maximumLength: 50, ErrorMessage = "the number of characters must be between 0 and 50")]
        public string Email { get; set; }

        public bool Active { get; set; }
    }
}
