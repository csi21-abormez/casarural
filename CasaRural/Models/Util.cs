﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace CasaRural.Models
{
    public class Util
    {
        public static List<Menu> Menus { get; set; } = new List<Menu>();
        public static string Encriptar(string clave)
        {
            string result;
            byte[] encryted = System.Text.Encoding.Unicode.GetBytes(clave);

            result = Convert.ToBase64String(encryted);
            return result;

        }

        public static void SendEmail(string emailTo, string subject, string body)
        {
            MailMessage email = new MailMessage();
            SmtpClient smtp = new SmtpClient();

            email.To.Add(new MailAddress(emailTo));
            email.From = new MailAddress("csi21-abormez@colegioaltair.net");
            email.Subject = subject;
            email.SubjectEncoding = System.Text.Encoding.UTF8;
            email.Body = body;
            email.IsBodyHtml = true;
            email.Priority = MailPriority.Normal;

            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential("csi21-abormez@colegioaltair.net", "altair21");
            

            smtp.Send(email);
            smtp.Dispose();
        }

        public static int TTL = 60;



        public static List<string> SelectListGenerator(string opciones)
        {
            List<string> opcionesSelect = new List<string>();
            var select = opciones.Split(",");
            for(int i = 0; i < select.Length; i++)
            {
                opcionesSelect.Add(select[i]);
            }
            return opcionesSelect;
        }

        public static byte[] GetByteArrayFromImage(IFormFile file)
        {
            using (var target = new MemoryStream())
            {
                file.CopyTo(target);
                return target.ToArray();
            }
        }
    }
}
