﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CasaRural.Models
{
    public class Role
    {
        public int ID { get; set; }
        [Display(Name = "Nombre")]
        [Required]
        [StringLength(50, ErrorMessage ="the field {0} can´t be longer than 50 characters")]
        public String Name { get; set; }
        [Display(Name = "Description")]
        [Required]
        [StringLength(500, ErrorMessage ="the field {0} can´t be longer than 500 characters")]

         public String Description { get; set; }

        public Boolean Active { get; set; }

        //Relationship UserAccount

        [InverseProperty("Role")]
        public List<UserAccount> ListUserAccount { get; set; }

        //Relationship RoleHasMenu

        [InverseProperty("Role")]

        public List<RoleHasMenu> ListRoleHasMenu { get; set; }
    }
}
