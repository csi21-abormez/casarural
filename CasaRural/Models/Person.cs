﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CasaRural.Models
{
    public abstract class Person
    {
        public int ID { get; set; }

        [Display(Name = "Name")]
        [Required]
        [StringLength(50, ErrorMessage = "the fiel {0} can´t be longer than 50 ")]
        public String Name { get; set; }

        [Required]
        [StringLength(80, ErrorMessage = "the fiel {0} can´t be longer than 80 ")]
        public String Surname { get; set; }
        [Required]
        [Phone(ErrorMessage = "the fiel {0} is not valid ")]
        public String Phone { get; set; }

        [Display(Name ="UserAccount")]
        public int UserAccountID { get; set; }

        [ForeignKey("UserAccountID")]
        public UserAccount UserAccount { get; set; }
    }
}
