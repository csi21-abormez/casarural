﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CasaRural.Models
{
    public class Inventory
    {
        public int ID { get; set; }

        public Boolean Active { get; set; }


        //Relationship Furniture
        [Display(Name = "Furniture")]
        public int FurnitureID { get; set; }
        [ForeignKey("FurnitureID")]
        public Furniture Furniture { get; set; }

        //Relationship Furniture

        [Display(Name = "Room")]
        public int RoomID { get; set; }
        [ForeignKey("RoomID")]
        public Room Room { get; set; }









    }
}
