﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CasaRural.Models
{
    public class TypeRoom
    {
        public int ID { get; set; }

        public Boolean Active { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The field {0} can't be longer than 100 characters")]
        public String Name { get; set; }
        [Required]
        [StringLength(500, ErrorMessage = "The field {0} can't be longer than 500 characters")]
        public String Description { get; set; }
        public double Price { get; set; }

    }
}
