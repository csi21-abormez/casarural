﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CasaRural.Models
{
    public class Other
    {
        public int ID { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The field {0} can't be longer than 100 characters")]
        public String Name { get; set; }
        public Double Price { get; set; }

        public Boolean Active { get; set; }

        //Relationship OtherHasSupplement

        [InverseProperty("Other")]
        public List<Supplement> ListSupplements { get; set; }
    }
}
