﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CasaRural.Models
{
    public class Furniture
    {
        public int ID { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The field {0} can't be longer than 100 characters")]
        public String Name { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime BuyDate { get; set; }
        public Double High { get; set; }
        public Double Weight { get; set; }
        public String Image { get; set; }
        public Boolean Available { get; set; }
        public Boolean Active { get; set; }


        //Relationship Furniture
        [InverseProperty("Furniture")]
        List<Inventory> ListInventory { get; set; }

    }
}
