﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CasaRural.Models
{
    public class RoleHasMenu
    {
        public int ID { get; set; }

        public Boolean Active { get; set; }
        [Display(Name ="Menu")]

        public int MenuID { get; set; }
       [ForeignKey("MenuID")]
        public Menu Menu { get; set; }
        [Display(Name = "Role")]
        public int RoleID { get; set; }
        [ForeignKey("RoleID")]

        public Role Role { get; set; }
    }
}
