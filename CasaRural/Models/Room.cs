﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CasaRural.Models
{
    public class Room
    {
        public int ID { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "The field {0} can't be longer than 50 characters")]
        public String Name { get; set; }
        public int NumberRoom { get; set; }
        public String Image { get; set; }

        public Boolean Active { get; set; }

        //Relationship Booking

        [InverseProperty("Room")]
        public List<Booking> ListBooking { get; set; }

        //Relationship RoomCleaning
        [InverseProperty("Room")]
        public List<RoomCleaning> ListRoomCleaning { get; set; }

        //Relationship TypeRoom

        [Display(Name = "TypeRoom")]
        public int TypeRoomID { get; set; }
        [ForeignKey("TypeRoomID")]
        public TypeRoom TypeRoom { get; set; }

        //Relationship InventoryHasRoom

        [InverseProperty("Room")]

        List<Inventory> ListInventory { get; set; }

       
    }
}
