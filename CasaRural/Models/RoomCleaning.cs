﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CasaRural.Models
{
    public class RoomCleaning
    {
        public int ID { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime CleanDate { get; set; }
        [StringLength(50, ErrorMessage = "The field {0} can't be longer than 50 characters")]
        public String Observations { get; set; }
        public Boolean Clean { get; set; }

        public Boolean Active { get; set; }


        //Relationship Employed

        [Display(Name = "Employed")]
        public int EmployedID { get; set; }
        [ForeignKey("EmployedID")]
        public Employed Employed { get; set; }

        //Relationship Room

        [Display(Name = "Room")]
        public int RoomID { get; set; }
        [ForeignKey("RoomID")]
        public Room Room { get; set; }
    }
}
