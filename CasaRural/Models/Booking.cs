﻿using CasaRural.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CasaRural.Models
{
    public class Booking
    {
        public int ID { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime BookingStartDate { get; set; }
        [Required]
        [DataType(DataType.Date)]

        public DateTime BookingEndDate { get; set; }

        public Boolean Active { get; set; }



        //Relationship Customer

        [Display(Name = "Customer")]
        public int CustomerID { get; set; }
        [ForeignKey("CustomerID")]

        public Customer Customer { get; set; }

        //Relationship Room

        [Display(Name = "Room")]
        public int RoomID { get; set; }
        [ForeignKey("RoomID")]

        public Room Room { get; set; }

        //Relationship Diet
        [Display(Name = "Diet")]
        public int DietID { get; set; }

        [ForeignKey("DietID")]
        public Diet Diet { get; set; }

        //Relationship Supplement
        [InverseProperty("Room")]
        List<Supplement> ListSupplement { get; set; }





    }
}
