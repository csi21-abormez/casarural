﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CasaRural.Models
{
   

    public class UserAccount
    {
        public int ID { get; set; }
        [Required (ErrorMessage = "it can´t be empty")]
        [StringLength(10, MinimumLength = 5, ErrorMessage = "the number of characters must be between 5 and 10")]
        
        public String Username { get; set; }

        [Required(ErrorMessage = "it can´t be empty")]
        
        [DataType(DataType.Password)]
       
        public String Password { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(maximumLength:50, ErrorMessage = "the number of characters must be between 0 and 50")]
        public String Email { get; set; }
        
        public bool Active { get; set; }

        //Relationship Role

        [Display(Name ="Role")]
        public int RoleID { get; set; }
        [ForeignKey("RoleID")]
        public Role Role { get; set; }
    }
}
