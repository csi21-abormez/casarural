﻿using CasaRural.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CasaRural.Models
{
    public class Customer:Person
    {

        [DataType(DataType.Date)]
        public DateTime Birthday { get; set; }

        [Required]
        [RegularExpression(@"^[0-9]{8}[A-Z]{1}$", ErrorMessage = "Document not valid.")]

        public String DNI { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The field {0} can't be longer than 100 characters")]


        public String Address { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The field {0} can't be longer than 100 characters")]


        public String City { get; set; }

        [Display(Name = "Postal Code")]
       
        [StringLength(50, ErrorMessage = "The field {0} can't be longer than 50 characters")]


        public String PostalCode { get; set; }



        //Relationship Booking

        [InverseProperty("Customer")]
        public List<Booking> ListBooking { get; set; }



    }
}
