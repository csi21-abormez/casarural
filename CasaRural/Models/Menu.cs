﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CasaRural.Models
{
    public class Menu
    {
        public int ID { get; set; }
        [Required]

        public String Controller { get; set; }
        [Required]

        public String Action { get; set; }
        [Required]

        public String Label { get; set; }

        public Boolean Active { get; set; }

        //Relationship RoleHasMenu
        [InverseProperty("Menu")]
        public List<RoleHasMenu> ListRoleHasMenu { get; set; }
        [NotMapped]
        public String Fullname { get => (Controller + "-" + Action); }
    }
}
