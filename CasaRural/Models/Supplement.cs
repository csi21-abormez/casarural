﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CasaRural.Models
{
    public class Supplement
    {
        public int ID { get; set; }

       

        //Relationship Other
        [Display(Name = "Other")]
        public int OtherID { get; set; }

        [ForeignKey("OtherID")]
        public Other Other { get; set; }

        public Boolean Active { get; set; }


        //Relationship Booking
        [Display(Name = "Booking")]
        public int BookingID { get; set; }

        [ForeignKey("BookingID")]
        public Booking Booking  { get; set; }


    }
}
