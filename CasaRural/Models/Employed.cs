﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CasaRural.Models
{
    public class Employed : Person
    {

        public int Age { get; set; }

        public Double  Salary { get; set; }



        //Relationship JobPosition

        [Display(Name = "Job Position")]
        public int JobPostionID { get; set; }
        [ForeignKey("JobPostionID")]
        public JobPosition JobPosition { get; set; }

        //Relationship RoomCleaning

        [InverseProperty("Employed")]
        public List<RoomCleaning> ListRoomCleaning { get; set; }


    }
}
